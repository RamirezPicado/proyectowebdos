<?php namespace MyApp\Models {

use EasilyPHP\Database\SqlMySQL;

class User
{
    private $db = null;

    public function __construct($config)
    {
      $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
    }

    /**
     * Obtiene todos los registros de usuario
     */
    public function getAllUsers()
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM users");
      $this->db->disconnect();
      return $this->db->getAll($result);
    }

    /**
     * Obtiene un registro de usuario
     */
    public function getUser($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM users WHERE id=".$id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }

    /**
     * Inserta un nuevo registro de usuario en base de datos
     * @param user contiene los datos del nuevo registro de usuario
     */
    public function insertUser($user)
    {
      // https://www.php.net/manual/es/mysqli.quickstart.prepared-statements.php
      // https://www.php.net/manual/es/mysqli-stmt.bind-param.php

      $this->db->connect();
      $sql = "INSERT INTO users (fullname, username, passwd) VALUES (?, ?, ?)";

      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("sss", $user['fullname'], $user['username'], $user['password']);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

    /**
    * Actualiza un registro de "user" en la base de datos
    * @param user contiene los datos del registro de usuario a actualizar
    */
    public function updateUser($user)
    {
      $this->db->connect();
      if ($user['password'] == "")
        $sql = "UPDATE users SET fullname = ?, username = ? WHERE id = ?";
      else
        $sql = "UPDATE users SET fullname = ?, username = ?, password = ? WHERE id = ?";

      $stmt = $this->db->prepareSQL($sql);
      if($stmt) {
        if ($user['password'] == "")
          $stmt->bind_param("ssi", $user['fullname'], $user['username'], $user['id']);
        else
          $stmt->bind_param("sssi", $user['fullname'], $user['username'], $user['password'], $user['id']);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

    /**
    * Elimina un registro de la base de datos
    * @param id del registro que se desea eliminar
    */
    public function deleteUser($id)
    {
      $this->db->connect();
      $sql = "DELETE FROM users WHERE id = ?";

      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

    public function authenticate($username, $password)
    {
      // Consulta para validar credenciales
      $sql = "SELECT id, username, fullname ,role FROM users " .
      " WHERE username = '". $username . "'" .
      "   AND passwd = '". $password . "'";

      $this->db->connect();
      $result = $this->db->runSql($sql);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }
}
}
