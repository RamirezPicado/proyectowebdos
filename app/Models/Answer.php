<?php namespace MyApp\Models {

use EasilyPHP\Database\SqlMySQL;

class Answer
{
    private $db = null;

    public function __construct($config)
    {
      $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
    }

    /**
     * Obtiene todos los registros de usuario
     */
    public function getAll($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM answers where question_id=".$id);

      $this->db->disconnect();

      return $this->db->getAll($result);
    }
    /**
     * Obtiene un registro de usuario
     */
    public function getById($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM answers WHERE id=".$id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }

    /**
     * Inserta un nuevo registro de usuario en base de datos
     * @param user contiene los datos del nuevo registro de usuario
     */
    public function insert($data)
    {
      // https://www.php.net/manual/es/mysqli.quickstart.prepared-statements.php
      // https://www.php.net/manual/es/mysqli-stmt.bind-param.php

      $this->db->connect();
      $sql = "INSERT INTO answers (question_id, number,answer_text ,answer_points) VALUES (?,?,?,?)";

      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("ssss", $data['question_id'], $data['number'],$data['answer_text'], $data['answer_points']);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

    /**
    * Actualiza un registro de "user" en la base de datos
    * @param user contiene los datos del registro de usuario a actualizar
    */
    public function update($data)
    {
      $this->db->connect();

        $sql = "UPDATE answers SET question_id = ?, number = ?, answer_text = ?,answer_points = ? WHERE id = ?";

      $stmt = $this->db->prepareSQL($sql);
      if($stmt) {

        $stmt->bind_param("sssss",  $data['question_id'], $data['number'],$data['answer_text'], $data['answer_points'], $data['id']);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

    /**
    * Elimina un registro de la base de datos
    * @param id del registro que se desea eliminar
    */
    public function delete($id)
    {
      $this->db->connect();
      $sql = "DELETE FROM answers WHERE id = ?";

      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

}
}
