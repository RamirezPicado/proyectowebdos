<?php namespace MyApp\Models {

use EasilyPHP\Database\SqlMySQL;

class Questionnaire
{
    private $db = null;

    public function __construct($config)
    {
      $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
    }


    /**
     * Obtiene todos los registros de usuario
     */
    public function getAllQuestionnairies()
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM questionnaires");
      $this->db->disconnect();
      return $this->db->getAll($result);
    }
    /**
     * Obtiene un registro de usuario
     */
    public function getQuestionnaire($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM questionnaires WHERE id=".$id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }

    /**
     * Inserta un nuevo registro de usuario en base de datos
     * @param user contiene los datos del nuevo registro de usuario
     */
    public function insertQuestionnaire($questionnairie)
    {
      // https://www.php.net/manual/es/mysqli.quickstart.prepared-statements.php
      // https://www.php.net/manual/es/mysqli-stmt.bind-param.php
      $this->db->connect();
      $sql = "INSERT INTO questionnaires (description, long_description) VALUES (?, ?)";

      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("ss", $questionnairie['description'], $questionnairie['long_description']);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();



    }



    /**
    * Actualiza un registro de "user" en la base de datos
    * @param user contiene los datos del registro de usuario a actualizar
    */
    public function updateQuestionnairie($questionnairie)
    {
      $this->db->connect();

        $sql = "UPDATE questionnaires SET description = ?, long_description = ? WHERE id = ?";

      $stmt = $this->db->prepareSQL($sql);
      if($stmt) {

        $stmt->bind_param("sss", $questionnairie['description'], $questionnairie['long_description'], $questionnairie['id']);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

    /**
    * Elimina un registro de la base de datos
    * @param id del registro que se desea eliminar
    */
    public function deleteQuestionnairie($id)
    {
      $this->db->connect();
      $sql = "DELETE FROM questionnaires WHERE id = ?";
      $sql1 = "DELETE FROM questions WHERE id = ?";
      $sql2 = "DELETE FROM answers WHERE question_id = ?";

      if($stmt = $this->db->prepareSQL($sql2)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }

      if($stmt = $this->db->prepareSQL($sql1)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }


      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();



    }


    public function authenticate($username, $password)
    {
      // Consulta para validar credenciales
      $sql = "SELECT id, username, fullname FROM users " .
      " WHERE username = '". $username . "'" .
      "   AND passwd = '". $password . "'";

      $this->db->connect();
      $result = $this->db->runSql($sql);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }
}
}