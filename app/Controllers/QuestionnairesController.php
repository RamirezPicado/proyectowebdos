<?php namespace MyApp\Controllers {

use MyApp\Models\Questionnaire;
use MyApp\Utils\Message;

class QuestionnairesController
{
    private $config = null;
    private $login = null;
    private $questionnaireModel = null;

    /**
     * Constructor de controlador de cuestionarios
     *
     * @param config contiene la configuración para la conexión de BD
     * @param login contiene los datos del cuestionario que inició sesión
     */
    public function __construct($config, $login)
    {
        $this->config = $config;
        $this->login = $login;
    }

    /**
     * Muestra la vista "Index" del catálogo de cuestionarios
     */
    public function index(){
      // $questionnaireModel = new Questionnaire($this->config);
      // $collection = $questionnaireModel->getAllQuestionnairies();
      //
      // $dataToView = [ "login" => $this->login ];
      // return view("questionnaires/index.php", $dataToView);


       $questionnaireModel = new Questionnaire($this->config);
      $collection = $questionnaireModel->getAllQuestionnairies();

      $dataToView = ["collection" => $collection,
                     "login" => $this->login ];

      return view("questionnaires/index.php", $dataToView);
    }

    /**
     * Muestra el formulario para crear un nuevo registro
     */
    public function create()
    {
        $dataToView = [ "login" => $this->login ];
        return view("questionnaires/new.php", $dataToView);
    }

    /**
     * Almacena un registro de "questionnaire" en la base de datos
     * @param request contiene los datos del nuevo registro de cuestionario
     */
    public function store($request)
    {
      $questionnaireModel = new Questionnaire($this->config);
   $questionnaireModel->insertQuestionnaire($request);
   header('Location: /questionnaires');

    }

    /**
     * Muestra la vista "show" con los detalles del registro "id"
     * @param id del registro del cual se deben mostrar los detalles
     */
    public function show($id)
    {
      $userModel = new Questionnaire($this->config);
      $item = $userModel->getQuestionnaire($id);
      $dataToView = ["login" => $this->login,
                     "item" => $item ];
      return view("questionnaires/show.php", $dataToView);
    }

    /**
    * Muestra la vista "edit" con los detalles del registro "id"
    * @param id del registro del cual se deben cargar los detalles para edición
    */
    public function edit($id)
    {
      $userModel = new Questionnaire($this->config);
      $item = $userModel->getQuestionnaire($id);
      $dataToView = ["login" => $this->login,
                     "item" => $item ];
      return view("questionnaires/edit.php", $dataToView);
    }

    /**
    * Actualiza un registro "questionnaire" en la base de datos
    * @param request contiene los datos del registro de cuestionario a actualizar
    */
    public function update($request)
    {

      $userModel = new Questionnaire($this->config);
      $userModel->updateQuestionnairie($request);

      //$this->index();
      header('Location: /questionnaires');

    }

    /**
     * Elimina un registro de la base de datos
     * @param id del registro que se desea eliminar
     */
    public function destroy($id)
    {
      $questionnaireModel = new Questionnaire($this->config);
      $questionnaireModel->deleteQuestionnairie($id);
      header('Location: /questionnaires');
    }
}
}
