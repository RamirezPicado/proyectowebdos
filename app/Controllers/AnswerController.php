<?php namespace MyApp\Controllers {

use MyApp\Models\Answer;
use MyApp\Utils\Message;

class AnswersController
{
    private $config = null;
    private $login = null;
    private $answerModel = null;

    /**
     * Constructor de controlador de cuestionarios
     *
     * @param config contiene la configuración para la conexión de BD
     * @param login contiene los datos del cuestionario que inició sesión
     */
    public function __construct($config, $login)
    {
        $this->config = $config;
        $this->login = $login;
    }

    /**
     * Muestra la vista "Index" del catálogo de cuestionarios
     */
    public function index($id){

      $answerModel = new Answer($this->config);
      $collection = $answerModel->getAll($id);

      $dataToView = ["collection" => $collection,
                     "login" => $this->login ];

      return view("answers/index.php", $dataToView);
    }

    /**
     * Muestra el formulario para crear un nuevo registro
     */
    public function create()
    {
        $dataToView = [ "login" => $this->login ];
        return view("answers/new.php", $dataToView);
    }

    /**
     * Almacena un registro de "questionnaire" en la base de datos
     * @param request contiene los datos del nuevo registro de cuestionario
     */
    public function store($request)
    {
     
      $answerModel = new Answer($this->config);
      $answerModel->insert($request);
      header('Location: /answers');
    }

    /**
     * Muestra la vista "show" con los detalles del registro "id"
     * @param id del registro del cual se deben mostrar los detalles
     */
    public function show($id)
    {
      $answerModel = new Answer($this->config);
      $item = $answerModel->getById($id);
      $dataToView = ["login" => $this->login,
                     "item" => $item ];
      return view("answers/show.php", $dataToView);
    }

    /**
    * Muestra la vista "edit" con los detalles del registro "id"
    * @param id del registro del cual se deben cargar los detalles para edición
    */
    public function edit($id)
    {
      $answerModel = new Answer($this->config);
      $item = $answerModel->getById($id);
      $dataToView = ["login" => $this->login,
                     "item" => $item ];
      return view("answers/edit.php", $dataToView);
    }

    /**
    * Actualiza un registro "questionnaire" en la base de datos
    * @param request contiene los datos del registro de cuestionario a actualizar
    */
    public function update($request)
    {

      $answerModel = new Answer($this->config);
      $answerModel->update($request);

      header('Location: /answers/index.php?question='.$request['question_id']);

    }

    /**
     * Elimina un registro de la base de datos
     * @param id del registro que se desea eliminar
     */
    public function destroy($id)
    {
      $answerModel = new Answer($this->config);
      $answerModel->delete($id);
      header('Location: /questionnaires/index.php');
    }
}
}
