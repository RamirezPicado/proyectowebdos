<?php namespace MyApp\Controllers {

use MyApp\Models\Question;
use MyApp\Utils\Message;

class QuestionsController
{
    private $config = null;
    private $login = null;
    private $questioneModel = null;

    /**
     * Constructor de controlador de cuestionarios
     *
     * @param config contiene la configuración para la conexión de BD
     * @param login contiene los datos del cuestionario que inició sesión
     */
    public function __construct($config, $login)
    {
        $this->config = $config;
        $this->login = $login;
    }

    /**
     * Muestra la vista "Index" del catálogo de cuestionarios
     */
    public function index($id){

      $QuestionModel = new Question($this->config);
      $collection = $QuestionModel->getAll($id);

      $dataToView = ["collection" => $collection,
                      "id"=>$id,
                     "login" => $this->login ];
      return view("questions/index.php", $dataToView);
    }

    /**
     * Muestra el formulario para crear un nuevo registro
     */
    public function create()
    {
        $dataToView = [ "login" => $this->login ];
        return view("questions/new.php", $dataToView);
    }

    /**
     * Almacena un registro de "questionnaire" en la base de datos
     * @param request contiene los datos del nuevo registro de cuestionario
     */
    public function store($request)
    {
      $questioneModel = new Question($this->config);
      $questioneModel->insert($request);
      header('Location: /questions/index.php?questionnaire='.$request['questionnaire_id']);
    }

    /**
     * Muestra la vista "show" con los detalles del registro "id"
     * @param id del registro del cual se deben mostrar los detalles
     */
    public function show($id)
    {
      $QuestionModel = new Question($this->config);
      $item = $QuestionModel->getById($id);
      $dataToView = ["login" => $this->login,
                     "item" => $item ];
      return view("questions/show.php", $dataToView);
    }

    /**
    * Muestra la vista "edit" con los detalles del registro "id"
    * @param id del registro del cual se deben cargar los detalles para edición
    */
    public function edit($id)
    {
      $QuestionModel = new Question($this->config);
      $item = $QuestionModel->getById($id);
      $dataToView = ["login" => $this->login,
                     "item" => $item ];
      return view("questions/edit.php", $dataToView);
    }

    /**
    * Actualiza un registro "questionnaire" en la base de datos
    * @param request contiene los datos del registro de cuestionario a actualizar
    */
    public function update($request)
    {

      $QuestionModel = new Question($this->config);
      $QuestionModel->update($request);

      header('Location: /questions/index.php?questionnaire='.$request['questionnaire_id']);

    }

    /**
     * Elimina un registro de la base de datos
     * @param id del registro que se desea eliminar
     */
    public function destroy($id,$questionnaire)
    {

      $QuestionModel = new Question($this->config);
      $QuestionModel->delete($id);
      header('Location: /questions/index.php?questionnaire='.$questionnaire);
    }
}
}
