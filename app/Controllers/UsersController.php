<?php namespace MyApp\Controllers {

    use MyApp\Models\User;
    use MyApp\Utils\Message;

    class UsersController
    {
        private $config = null;
        private $login = null;
        private $userModel = null;

        /**
         * Constructor de controlador de usuarios
         *
         * @param config contiene la configuración para la conexión de BD
         * @param login contiene los datos del usuario que inició sesión
         */
        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        /**
         * Muestra la vista "Index" del catálogo de usuarios
         */
        public function index(){
            $userModel = new User($this->config);
            $collection = $userModel->getAllUsers();

            $dataToView = ["collection" => $collection,
                           "login" => $this->login ];

            return view("users/index.php", $dataToView);
        }

        /**
         * Muestra el formulario para crear un nuevo registro
         */
        public function create()
        {
            $dataToView = [ "login" => $this->login ];
            return view("users/new.php", $dataToView);
        }

        /**
         * Almacena un registro nuevo de "user" en la base de datos
         * @param request contiene los datos del nuevo registro de usuario
         */
        public function store($request)
        {
            $userModel = new User($this->config);
            $userModel->insertUser($request);
            header('Location: /users');
        }

        /**
         * Muestra la vista "show" con los detalles del registro "id"
         * @param id del registro del cual se deben mostrar los detalles
         */
        public function show($id){
            $userModel = new User($this->config);
            $user = $userModel->getUser($id);
            $dataToView = ["login" => $this->login,
                           "user" => $user ];
            return view("users/show.php", $dataToView);
        }

        /**
        * Muestra para vista "edit" con los detalles del registro "id"
        * @param id del registro del cual se deben cargar los detalles para edición
        */
        public function edit($id){
            $userModel = new User($this->config);
            $user = $userModel->getUser($id);
            $dataToView = ["login" => $this->login,
                           "user" => $user ];
            return view("users/edit.php", $dataToView);
        }

        /**
        * Actualiza un registro de "user" en la base de datos
        * @param request contiene los datos del registro de usuario a actualizar
        */
        public function update($request)
        {
            $userModel = new User($this->config);
            $userModel->updateUser($request);
            //$this->index();
            header('Location: /users');
        }

        /**
         * Elimina un registro de la base de datos
         * @param id del registro que se desea eliminar
         */
        public function destroy($id)
        {
            $userModel = new User($this->config);
            $userModel->deleteUser($id);
            header('Location: /users');
        }


        /**
         * Almacena un registro nuevo de "user" en la base de datos
         * @param request contiene los datos del nuevo registro de usuario
         */
        public function register()
        {
            $dataToView = [ "login" => $this->login ];
            return view("users/register.php", $dataToView);
        }

        /**
         * Almacena un registro nuevo de "user" en la base de datos
         * @param request contiene los datos del nuevo registro de usuario
         */
        public function store2($request)
        {
            $userModel = new User($this->config);
            $userModel->insertUser($request);
            header('Location: /login');
        }
    }

}
