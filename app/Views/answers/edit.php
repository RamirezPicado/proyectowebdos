<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-sm-6">
        <h1>Editar Pregunta</h1>
        <form action="/answers/index.php?action=update" method="post">
          <input type="hidden" name="id" value="<?= $item["id"]; ?>">
          <input type="hidden" name="question_id" value="<?= $item["question_id"]; ?>">
          <div class="form-group">
            <label for="fullname">Respuesta</label>
            <input
              type="text" class="form-control" id="answer_text" name="answer_text"
              value="<?php echo $item["answer_text"]; ?>">
          </div>
          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/answers/index.php?question=<?php echo $item['question_id'] ?>">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>


