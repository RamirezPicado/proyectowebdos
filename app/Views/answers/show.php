<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-sm-6">
        <h1>Ver respuesta</h1>
        <form>
          <div class="form-group">
            <label for="fullname">Respuesta</label>
            <input
              type="text" class="form-control" id="answer_text"
              value="<?php echo $item["answer_text"]; ?>" readonly>
          </div>

          <a class="btn btn-primary"
            href=<?= "/answers/index.php?edit=".$item["id"]?>>Editar</a>
          <a class="btn btn-secondary" href="/answers/index.php?question=<?php echo $item['question_id'] ?>">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
