<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-sm-6">
        <h1>Editar Pregunta</h1>
        <form action="/questions/index.php?action=update" method="post">
          <input type="hidden" name="id" value="<?= $item["id"]; ?>">
          <input type="hidden" name="questionnaire_id" value="<?= $item["questionnaire_id"]; ?>">
          <div class="form-group">
            <label for="fullname">Pregunta</label>
            <input
              type="text" class="form-control" id="question_text" name="question_text"
              value="<?php echo $item["question_text"]; ?>">
          </div>
          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/questions/index.php?questionnaire=<?php echo $item['questionnaire_id'] ?>">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
