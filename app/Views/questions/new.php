<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <?php include VIEWS.'/partials/message.php' ?>
    <div class="row">
      <div class="col-sm-6">
        <h1>Agregar Pregunta</h1>
        <form action="/questions/index.php?action=save" method="post">
          <div class="form-group">
            <label for="questionnaire_id">Pregunta Id</label>
            <input 
              type="text" class="form-control" id="questionnaire_id" name="questionnaire_id">
          </div>
          <div class="form-group">
            <label for="question_text">Texto Pregunta</label>
            <input 
              type="text" class="form-control" id="question_text" name="question_text">
          </div>
          
          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/question/index.php">Regresar</a>
         
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>

