<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-sm-6">
        <h1>Ver Questionario</h1>
        <form>
          <div class="form-group">
            <label for="fullname">Pregunta</label>
            <input
              type="text" class="form-control" id="question_text"
              value="<?php echo $item["question_text"]; ?>" readonly>
          </div>

          <a class="btn btn-primary"
            href=<?= "/questions/index.php?edit=".$item["id"]?>>Editar</a>
          <a class="btn btn-secondary" href="/questions/index.php?questionnaire=<?php echo $item['questionnaire_id'] ?>">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
