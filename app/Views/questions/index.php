<?php
  include VIEWS.'/partials/header.php';
  include VIEWS.'/partials/navbar.php';
?>
  <div class="container"><br>
    <div class="row">

      <div class="col-sm-12">

        <form class="form-inline" action="/questions/index.php?action=save" method="post">
          <input type="hidden" id="questionnaire_id" name="questionnaire_id" value="<?php echo $id; ?>">

        <div class="form-group mx-sm-3 mb-2">
          <label for="inputPassword2" class="sr-only">Pregunta</label>
          <input type="text" class="form-control" name="question_text" id="question_text" placeholder="Pregunta" required>
        </div>

  <button type="submit" class="btn btn-primary mb-2">Agregar Pregunta</button>
  </form>

      </div>
      <div class="col-sm-12">
        <h1>Preguntas</h1>
        <table class="table table-striped">
          <thead>
            <tr>
              <th class="text-center">Ver</th>
              <th class="text-center">Editar</th>
              <th class="text-center">Eliminar</th>
              <th scope="col">Descripción</th>
              <th scope="col">Respuestas</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($collection as $item): ?>
            <tr>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-eye" href="<?= "/questions/index.php?show=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-edit" href="<?= "/questions/index.php?edit=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-trash" href="<?= "/questions/index.php?delete=".$item['id'].'&'.'id='.$id; ?>"></a>
              </td>
              <td><?= $item['question_text']; ?></td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-question" href="<?="/answers/index.php?question=".$item['id']; ?>"></a>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
