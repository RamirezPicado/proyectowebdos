<?php
  include VIEWS.'/partials/header.php';
  include VIEWS.'/partials/navbar.php';
?>
  <div class="container"><br>
  <?php include VIEWS.'/partials/message.php' ?>
  <div class="row">
        <div class="col-sm-6">
          <h1>Login</h1>
          <!-- Inicia el formulario de Login -->
          <form action="/authenticate/index.php?action=auth" method="post">
            <div class="form-group">
              <label for="username">Usuario:</label>
              <input
                type="text" class="form-control"
                id="username" name="username"
                aria-describedby="Introduzca su nombre de usuario"
                placeholder="" required>
            </div>
            <div class="form-group">
              <label for="password">contraseña:</label>
              <input type="password" class="form-control" required
                id="password" name="password"
                placeholder="">
            </div>
            <button type="submit" class="btn btn-primary">Iniciar sesión</button>
            <a class="btn btn-primary" href="/" role="button">Cancelar</a>

          </form>
        </div>
      </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
