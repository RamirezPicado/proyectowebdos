<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <?php include VIEWS.'/partials/message.php' ?>
    <div class="row">
      <div class="col-sm-6">
        <h1>Agregar Usuario</h1>
        <form action="/users/index.php?action=save" method="post">
          <div class="form-group">
            <label for="fullname">Nombre completo</label>
            <input 
              type="text" class="form-control" id="fullname" name="fullname">
          </div>
          <div class="form-group">
            <label for="username">Nombre de usuario</label>
            <input 
              type="text" class="form-control" id="username" name="username">
          </div>
          <div class="form-group">
            <label for="password">Contraseña</label>
            <input 
              type="text" class="form-control" id="password" name="password">
          </div>
          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/users/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>