<?php
  include VIEWS.'/partials/header.php';
  include VIEWS.'/partials/navbar.php';
?>
  <div class="container"><br>
  <?php include VIEWS.'/partials/message.php' ?>
  <div class="row">
        <div class="col-sm-6">
          <h1>Registro</h1>
          <!-- Inicia el formulario de Login -->
          <form action="/register/index.php?action=save" method="post">
            <div class="form-group">
              <label for="fullname">Nombre Completo:</label>
              <input
                type="text" class="form-control"
                id="fullname" name="fullname"
                aria-describedby="Introduzca el Nombre Completo"
                placeholder="" required>
            </div<div class="form-group">
              <label for="username">Usuario:</label>
              <input
                type="text" class="form-control"
                id="username" name="username"
                aria-describedby="Introduzca el Usuario"
                placeholder="" required>
            </div>

            <div class="form-group">
              <label for="password">contraseña:</label>
              <input type="password" class="form-control"
                id="password" name="password"
                placeholder="" required>
            </div>

             <div class="form-group">
              <label for="password">Confirmar Contraseña:</label>
              <input type="password" class="form-control"
                id="password" name="password"
                placeholder="" required>
            </div>
            <button type="submit" class="btn btn-primary">Registrar</button>
            <a class="btn btn-primary" href="/" role="button">Cancelar</a>
          </form>
        </div>
      </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
