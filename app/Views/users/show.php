<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-sm-6">
        <h1>Ver Usuario</h1>
        <form>
          <div class="form-group">
            <label for="fullname">Nombre completo</label>
            <input 
              type="email" class="form-control" id="fullname"
              value="<?php echo $user["fullname"]; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="username">Nombre de usuario</label>
            <input 
              type="email" class="form-control" id="username"
              value="<?= $user["username"]; ?>" readonly>
          </div>
          <a class="btn btn-primary" 
            href=<?= "/users/index.php?edit=".$user["id"]?>>Editar</a>
          <a class="btn btn-secondary" href="/users/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?