<?php
  include VIEWS.'/partials/header.php';
  include VIEWS.'/partials/navbar.php';
?>
  <div class="container"><br>
    <div class="row">
      <div class="col-sm-12">
        <h1>Usuarios</h1>
        <table class="table table-striped">
          <thead>
            <tr>
              <th class="text-center">Ver</th>
              <th class="text-center">Editar</th>
              <th class="text-center">Eliminar</th>
              <th scope="col">Id</th>
              <th scope="col">Fullname</th>
              <th scope="col">Username</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($collection as $item): ?>
            <tr>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-eye" href="<?= "/users/index.php?show=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-edit" href="<?= "/users/index.php?edit=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-trash" href="<?= "/users/index.php?delete=".$item['id']; ?>"></a>
              </td>
              <td><?= $item['id']; ?></td>
              <td><?= $item['fullname']; ?></td>
              <td><?= $item['username']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
  

      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
