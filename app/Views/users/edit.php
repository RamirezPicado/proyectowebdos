<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-sm-6">
        <h1>Editar Usuario</h1>
        <form action="/users/index.php?action=update" method="post">
          <input type="hidden" name="id" value="<?= $user["id"]; ?>">
          <div class="form-group">
            <label for="fullname">Nombre completo</label>
            <input
              type="text" class="form-control" id="fullname" name="fullname"
              value="<?php echo $user["fullname"]; ?>">
          </div>
          <div class="form-group">
            <label for="username">Nombre de usuario</label>
            <input
              type="text" class="form-control" id="username" name="username"
              value="<?= $user["username"]; ?>">
          </div>
          <div class="form-group">
            <label for="role">Rol</label>
            <select class="form-control" name="role">
              <option value="R"  <?php if($user["role"] == 'R'){ echo ' selected="selected"'; } ?>>Usuario</option>
              <option value="S" <?php if($user["role"] == 'S'){ echo ' selected="selected"'; } ?>>Admin</option>
            </select>
            </div>
            <div class="form-group">
              <label for="role">Estado</label>
              <select class="form-control" name="blocked">
                <option value="Y" <?php if($user["blocked"] == 'Y'){ echo ' selected="selected"'; } ?>>Bloqueado</option>
                <option value="N" <?php if($user["blocked"] == 'N'){ echo ' selected="selected"'; } ?>>Desbloqueado</option>
              </select>
              </div>
          <div class="form-group">
            <label for="password">Nueva contraseña</label>
            <input
              type="text" class="form-control" id="password" name="password">
          </div>

          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/users/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
