<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-sm-6">
        <h1>Editar Questionario</h1>
        <form action="/questionnaires/index.php?action=update" method="post">
          <input type="hidden" name="id" value="<?= $item["id"]; ?>">
          <div class="form-group">
            <label for="fullname">Descripción</label>
            <input
              type="text" class="form-control" id="description" name="description"
              value="<?php echo $item["description"]; ?>">
          </div>
          <div class="form-group">
            <label for="username">Descripción larga</label>
            <input
              type="text" class="form-control" id="long_description" name="long_description"
              value="<?= $item["long_description"]; ?>">
          </div>


          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/questionnaires/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
