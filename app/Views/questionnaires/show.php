<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-sm-6">
        <h1>Ver Questionario</h1>
        <form>
          <div class="form-group">
            <label for="fullname">Descripción</label>
            <input
              type="email" class="form-control" id="fullname"
              value="<?php echo $item["description"]; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="username">Descripción larga</label>
            <input
              type="email" class="form-control" id="username"
              value="<?= $item["long_description"]; ?>" readonly>
          </div>
          <a class="btn btn-primary"
            href=<?= "/questionnaires/index.php?edit=".$item["id"]?>>Editar</a>
          <a class="btn btn-secondary" href="/questionnaires/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
