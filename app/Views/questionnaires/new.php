<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <?php include VIEWS.'/partials/message.php' ?>
    <div class="row">
      <div class="col-sm-6">
        <h1>Agregar Cuestionario</h1>
        <form action="/questionnaires/index.php?action=save" method="post">
          <div class="form-group">
            <label for="description">Descripción</label>
            <input 
              type="text" class="form-control" id="description" name="description">
          </div>
          <div class="form-group">
            <label for="long_description">Descripción Larga</label>
            <input 
              type="text" class="form-control" id="long_description" name="long_description">
          </div>
          
          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/questionnaires/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>