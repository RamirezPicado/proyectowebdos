<?php
  include VIEWS.'/partials/header.php';
  include VIEWS.'/partials/navbar.php';
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/functions.js"></script>

  <div class="container"><br>
    <div class="row">
      <div class="col-sm-12">
        <h1>Cuestionarios</h1>
        <table class="table table-striped">
          <thead>
            <tr>
              <?php if($login['role']=="S"): ?>
                <th class="text-center">Ver</th>
                <th class="text-center">Editar</th>
                <th class="text-center">Eliminar</th>
                <th class="text-center">Preguntas</th>
                <th class="text-center">Resultados</th>
              <?php endif; ?>
              <th scope="col">Descripción</th>
              <th scope="col">Largar Descripción</th>
              <?php if($login['role']=="R"):?>
                        <th scope="col">Responder</th>

              <?php endif; ?>
            </tr>
          </thead>
          <tbody>
        
            <?php foreach ($collection as $item): ?>

              <?php  if($login['role']=="S"): ?>

                <td class="text-center">
                    <a class="btn btn-sm btn-secondary fas fa-eye" href="<?= "/questionnaires/index.php?show=".$item['id']; ?>"></a>
                  </td>
                  <td class="text-center">
                    <a class="btn btn-sm btn-secondary fas fa-edit" href="<?= "/questionnaires/index.php?edit=".$item['id']; ?>"></a>
                  </td>
                  <td class="text-center">
                    <a class="btn btn-sm btn-secondary fas fa-trash" href="<?= "/questionnaires/index.php?deleteQuestionnairie=".$item['id']; ?>"></a>
                  </td>
                  <td class="text-center">
                    <a class="btn btn-sm btn-secondary fas fa-question" href="<?= "/questions/index.php?questionnaire=".$item['id']; ?>"></a>
                  </td>
                  <td class="text-center">
                    <a class="btn btn-sm btn-secondary fas fa-database" href="<?= "/questionnaires/index.php?=".$item['id']; ?>"></a>
                  </td>
              <?php endif; ?>
            
              <td><?= $item['description']; ?></td>
              <td><?= $item['long_description']; ?></td>
              <?php if($login['role']=="R"):?>
               <td class="text-center">
                  <a class="btn btn-sm btn-secondary fas fa-file-alt" href="/"></a>
                
              <?php endif; ?>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>


      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
