# Model–View–Controller on PHP

This is a very simple application to demonstrate some basic concepts of the architectural pattern of software Model–View–Controller (usually known as MVC).

This implementation is separated a bit from the classic model, adopting features of modern PHP frameworks, mainly the scheme where the model does not communicate directly with the view.

![Patron MVC](https://s3.amazonaws.com/coding-rs/images/mvc/basic_mvc_on_framework.png)


## Prerequisites

You must install `php` (PHP: Hypertext Preprocessor), the best way to do it on GNU/Linux is with `sudo apt-get install php`. The detailed installation of `php` is beyond the scope of this document.

## How to use ?

After cloning the repository, run the following commands to test the application.

To run the application on PHP build-in web server:

    php -S localhost:3000 -t public


### LICENSE

The files in this archive are released under the MIT license.
You can find a copy of this license in [LICENSE](LICENSE).
