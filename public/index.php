<?php
    // Esta es la parte pública de la vista "Dashboard"
    
    // En cada script hay que cargar el init.php
    include_once $_SERVER['DOCUMENT_ROOT']."/../app/init.php";

    // Usamos el helper de micro framework para
    // renderizar la vista "Dashboard"
    view("dashboard.php", compact("login"));