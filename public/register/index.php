<?php
    // Esta es el "dispatcher" de usuarios

    // En cada script hay que cargar el init.php
    include_once $_SERVER['DOCUMENT_ROOT']."/../app/init.php";

    // Se cargan las clases que se ocupan
    include_once CONTROLLERS."/UsersController.php";
    include_once MODELS."/User.php";


    // Se hace el "use" de las clases que se utilizaran en este script
    use MyApp\Controllers\UsersController;
    $controller = new UsersController($config, $login);

    // Si viene asignado el parámetro "show" se muestra la vista "show"


    // Otras acciones se capturan por medio del parámetro "action"


    // En cualquier otro caso se muestra la vista "index"

    // Otras acciones se capturan por medio del parámetro "action"
    if (isset($_GET['action'])){
        switch ($_GET['action']) {
            case 'new':
                $controller->register();
                //crear Levantar la vista
                break;
            case 'save':
                $controller->store2($_POST);
                // guardar
                break;

            default:
                break;
        }
    }
    $controller->register();